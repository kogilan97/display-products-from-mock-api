# Project Title

Simple React Native application to display, sort and filter products fetched from MockAPI

## Getting Started

### Installing
* Run 'yarn' to install the dependencies

#### Android
* Run 'react-native run-android'

#### IOS
* Run 'pod install'
* Run 'react-native run-ios'


