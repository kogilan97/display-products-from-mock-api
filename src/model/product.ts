export type Product = {
  product_name: string;
  product_category: string;
  product_price: number;
  product_shipping_cost: number;
  product_country_from: string;
  product_image: string;
};
