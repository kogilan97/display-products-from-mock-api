import {ViewProps} from 'react-native';

export type SearchComponentProps = ViewProps & {
  sortProducts: () => void;
  filterProducts: (keyword: string) => void;
};
