import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  searchInput: {
    flex: 1,
    marginLeft: 40,
    borderRadius: 10,
    marginRight: 15,
    backgroundColor: '#e3e1e1',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  searchView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 30,
  },

  sortText: {
    marginRight: 15,
    fontSize: 20,
    color: '#0000EE',
  },
});
