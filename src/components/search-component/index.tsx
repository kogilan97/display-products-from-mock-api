import _ from 'lodash';
import React from 'react';
import {View, TextInput, Text} from 'react-native';
import {SearchComponentProps} from './props';
import {styles} from './styles';

export const SearchComponent: React.FC<SearchComponentProps> = props => {
  const {filterProducts, sortProducts} = props;

  return (
    <View style={styles.searchView}>
      <TextInput
        onChange={({nativeEvent: {text}}) => filterProducts(text)}
        style={styles.searchInput}
      />
      <Text onPress={sortProducts} style={styles.sortText}>
        Sort
      </Text>
    </View>
  );
};
