import {Product} from '../../model/product';

export type ProductItemProps = Product;
