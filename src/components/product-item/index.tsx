import React, {FC} from 'react';
import {View, Image, Text} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {ProductItemProps} from './props';
import {styles} from './styles';

export const ProductItem: FC<ProductItemProps> = props => {
  const {
    product_name,
    product_category,
    product_price,
    product_country_from,
    product_image,
    product_shipping_cost,
  } = props;
  return (
    <View style={styles.individualView}>
      <Image
        borderRadius={20}
        resizeMode={'contain'}
        style={styles.imageView}
        source={{
          uri: product_image,
        }}
      />

      <View style={styles.contentView}>
        <Text numberOfLines={2} style={styles.productName}>
          {product_name}
        </Text>
        <Text numberOfLines={1} style={styles.productCategory}>
          {product_category}
        </Text>
        <Text numberOfLines={1} style={styles.productPrice}>
          {product_price.toFixed(2)}
        </Text>

        <Text numberOfLines={1} style={styles.productShippingCost}>
          {`+ USD ${product_shipping_cost.toFixed(2)} postage`}
        </Text>

        <Text numberOfLines={1} style={styles.productCountryFrom}>
          {`from ${product_country_from}`}
        </Text>

        <MaterialCommunityIcons
          style={styles.favouriteIcon}
          name="cards-heart-outline"
          size={30}
        />
      </View>
    </View>
  );
};
