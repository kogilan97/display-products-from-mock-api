import {Product} from '../../model/product';

export type FlatListProps = {
  productDetails: Product[];
};
