import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  imageView: {
    width: 150,
    height: 150,
  },

  individualView: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  separator: {height: 1, marginVertical: 10, backgroundColor: '#e3e1e1'},

  favouriteIcon: {
    alignSelf: 'flex-end',
    marginRight: 10,
  },
  contentView: {marginHorizontal: 10, flex: 1},

  productName: {
    color: '#000',
    fontSize: 20,
  },
  productPrice: {
    color: '#000',
    marginTop: 5,
    fontSize: 20,
    fontStyle: 'italic',
    fontWeight: 'bold',
  },
  productCategory: {color: '#808080'},

  productShippingCost: {
    color: '#808080',
    fontSize: 15,
    fontStyle: 'italic',
    fontWeight: 'bold',
  },

  productCountryFrom: {
    color: '#808080',
    fontSize: 15,
    fontStyle: 'italic',
    fontWeight: 'bold',
  },
});
