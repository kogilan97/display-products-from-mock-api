import React from 'react';
import {FlatList, ListRenderItem, View} from 'react-native';
import {Product} from '../../model/product';
import {FlatListProps} from './props';
import {styles} from './styles';
import {ProductItem} from '../product-item/index';

export const ProductFlatList: React.FC<FlatListProps> = ({productDetails}) => {
  const renderItem: ListRenderItem<Product> = ({item: product}) => {
    return <ProductItem {...product} />;
  };

  const renderSeparator = () => {
    return <View style={styles.separator} />;
  };
  return (
    <FlatList
      ItemSeparatorComponent={renderSeparator}
      data={productDetails}
      renderItem={renderItem}
    />
  );
};
