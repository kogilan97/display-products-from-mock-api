import baseAPI from './api-base';

export const fetchProductDetailsAction = () => {
  return baseAPI.get('/products');
};
