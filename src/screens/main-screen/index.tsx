import {View} from 'react-native';
import React, {useEffect, useReducer} from 'react';
import {Product} from '../../model/product';
import {fetchProductDetailsAction} from '../../network/api-interface';
import _ from 'lodash';
import {SearchComponent} from '../../components/search-component/index';
import {ProductFlatList} from '../../components/product-flatlist';
import {styles} from './styles';

const reducer = (state: any, action: any) => {
  //state === {productList:[], filteredProductList:[]}
  //action === {type: 'productList' || 'filteredProductList', payload: new Product List  }

  switch (action.type) {
    case 'setProductList':
      return {...state, productList: action.payload};
    case 'setFilteredProductList':
      return {...state, filteredProductList: action.payload};
    default:
      return state;
  }
};

const MainScreen: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, {
    productList: [],
    filteredProductList: [],
  });

  useEffect(() => {
    fetchProductDetailsAction()
      .then(response => {
        dispatch({type: 'setProductList', payload: response.data});
        dispatch({type: 'setFilteredProductList', payload: response.data});
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const filterProducts = (value: string) => {
    //Filter Based on the Typed Value
    const filteredProductList = _.filter(state.productList, item =>
      item.product_name.toLowerCase().startsWith(value.toLowerCase()),
    );
    dispatch({type: 'setFilteredProductList', payload: filteredProductList});
  };

  const sortProducts = () => {
    //Sort in Ascending Order
    dispatch({
      type: 'setFilteredProductList',
      payload: _.orderBy(state.filteredProductList, 'product_name', ['asc']),
    });
  };

  return (
    <View style={styles.parentView}>
      <SearchComponent
        filterProducts={filterProducts}
        sortProducts={sortProducts}
      />
      <ProductFlatList productDetails={state.filteredProductList} />
    </View>
  );
};

export default MainScreen;
